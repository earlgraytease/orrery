---
layout: dev
title: Engine Source
css_additional: 
 - /css/syntaxHighlight.css
---

Yes I know my commenting and documentation leaves a lot to be desired. Here's some repos, and the source code. It's definitely overengineered since I don't use half the properties I've set in AstroAddress. Not even all of the constructor options either.

[Site Repository](https://bitbucket.org/earlgraytease/orrery/src/master/)

[Backend Repository](https://github.com/dhulliath/ojtekapi)

## Engine Source Code

{% highlight js linenos %}
{% include_relative js/orreryEngine.js %}
{% endhighlight %}