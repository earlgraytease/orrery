---
layout: dev
title: Todo
---

# Todo List
I'll try to do these in top to bottom order.

### Aspect calculation and generation
This will likely be another onChange function added to each planetary AstroAddress. Anticipating trickiness fitting the Ascendant and Midheaven into this.

### Dynamic interpretation loading
I see no reason to stop with hooking functions onto the AstroAddress objects. Probably more of the same going to happen here. Another piece of trickery may be ensuring this fires after aspects are determined.

### Styling
As you can see from this bare-assed barely styled page, little to no effort has been put into the actual styling. Which is kinda offensive to me. But I've been busy on backend stuff.