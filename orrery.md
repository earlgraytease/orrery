---
layout: orrery
language: en
css_additional:
  - /css/orrery.css
js_additional:
  - /js/orreryEngine.js
  - https://code.jquery.com/jquery-3.3.1.min.js
---

### Chart Shortcuts

* <a href="javascript:orrery.chartRandom()">Random Chart</a>
* [Peter](?h=5gC09LlQBrVQEEyOH6zgLXzU&p=TAUyB2DgQqPoVmN6PpD62pbVXSy0Y8jgZblQSPDM0BI9GfoOBNGbg0S8GK6aa7wO)

[New Chart](/)

<script>

var orrery = new AstroGraphics(document.querySelector('#orrery'))
orrery.chartFromGet()
	
</script>